import { BUTTON_CREATE_NEW_ORDER, BUTTON_DELETE_HANDLER, BUTTON_EDIT_HANDLER, CHANGE_STATUS_DELETE, CHANGE_STATUS_ORDER_HANDLER, CONFIRM_CHANGE_STATUS_ORDER, CONFIRM_DELETE_ORDER_HANDLER, COUNTING_PAGE, FETCH_API_DRINK, FETCH_API_PENDING, FETCH_API_SUCCESS, FETCH_API_VOUCHER, GET_INFO_CREATE_NEW_ORDER, GET_USER_CREATE_NEW_ORDER, OPEN_TOAST, SET_ROW_PAGE, SET_STATUS_CREATE_MODAL, SET_STATUS_EDIT_MODAL } from "../constants/order.constants";

const initialState = {
    orders: [],
    pendingStatus : false,
    totalOrders: 0,
    page: 0,
    rowPerPage: 10,
    statusCreate: false,
    statusEdit: false,
    orderInfor: {},
    size: "",
    suonNuong: 0,
    salad: "",
    soLuongNuoc: 0,
    price: 0,
    pizzaSize:"",
    pizzaType: "",
    newOrder:[],
    drink: [],
    toast: {
        open: false,
        message: "",
        type: "success"
    },
    voucherId: "",
    newPrice: 0,
    statusOrder: "",
    statusDelete: false,

}
export default function orderReducer(state = initialState, action){
    switch (action.type) {
        // chỉnh status trong giai đoạn pending
        case FETCH_API_PENDING:
            state.pendingStatus = true;
            break;

        //khi get data api success
        case FETCH_API_SUCCESS:
            state.orders =  action.data;
            state.pendingStatus = false;
            state.totalOrders = action.data.length;
            break;

        //thiết lập số trang dựa trên số lượng user lấy về
        case COUNTING_PAGE:
            state.page = action.data;
            break;

        //thiết lập số hàng mỗi trang tùy chỉnh theo pagination
        case SET_ROW_PAGE:
            state.rowPerPage = action.data;
            state.page = 0
            break;

        //setting trạng thái mở modal create
        case SET_STATUS_CREATE_MODAL:
            state.statusCreate = action.data;
            break;

        //khi nút edit order được bấm trên table
        case BUTTON_EDIT_HANDLER:
            state.orderInfor =  action.data;
            state.statusEdit = true;
            state.statusOrder = state.orderInfor.trangThai
            break;

        //khi chọn trạng thái order
        case CHANGE_STATUS_ORDER_HANDLER:
            state.statusOrder = action.data;
            break;

        case SET_STATUS_EDIT_MODAL:
            state.statusEdit = action.data;
            break;

        //khi xác nhận thay đổi trạng thái order
        case CONFIRM_CHANGE_STATUS_ORDER:
            state.statusEdit = false;
            state.toast = {
                open: true,
                message: "Update Status Order Successfully",
                type: "success"
            }
            break;

        //khi nút delete order được bấm trên table
        case BUTTON_DELETE_HANDLER: 
            state.orderInfor = action.data;
            state.statusDelete = true;
            break;

        //khi bấm đóng trên modal delete
        case CHANGE_STATUS_DELETE:
            state.statusDelete = action.data;
            break;

        //Khi bấm xác nhận xóa trên modal delete
        case CONFIRM_DELETE_ORDER_HANDLER:
            state.statusDelete = false;
            state.pendingStatus = false;
            break;

        //lấy data drink từ api
        case FETCH_API_DRINK:
            state.drink = action.data;
            break;

        //thu thập thông tin order trong modal create new order
        case GET_INFO_CREATE_NEW_ORDER: 
            //đưa dữ liệu pizza size vào newOrder thông qua option được lựa chọn từ modal
            if(action.data.pizzaSize === "L"){
                state.newOrder.pizzaSize = {
                    duongKinh:"30 cm",
                    kichCo:"L",
                    soLuongNuoc: 4,
                    suon: 8,
                    thanhTien: 250000,
                    salad: "500 g"
                };
                    state.pizzaSize ="L"
                    state.size = "30 cm";
                    state.suonNuong = 8;
                    state.soLuongNuoc = 4;
                    state.salad = "500 g";
                    state.price = 250000;
                break;
            }
            if(action.data.pizzaSize === "S"){
                state.newOrder.pizzaSize = {
                    duongKinh:"20 cm",
                    kichCo:"S",
                    soLuongNuoc: 2,
                    suon: 2,
                    thanhTien: 150000,
                    salad: "200 g"
                };
                    state.pizzaSize ="S"
                    state.size = "20 cm";
                    state.suonNuong = 2;
                    state.soLuongNuoc = 2;
                    state.salad = "200 g";
                    state.price = 150000;
                break;
            }
            if(action.data.pizzaSize === "M"){
                state.newOrder.pizzaSize = {
                    duongKinh:"25 cm",
                    kichCo:"M",
                    soLuongNuoc: 3,
                    suon: 4,
                    thanhTien: 200000,
                    salad: "300 g"
                };
                    state.pizzaSize ="M"
                    state.size = "25 cm";
                    state.suonNuong = 4;
                    state.soLuongNuoc = 3;
                    state.salad = "300 g";
                    state.price = 200000;
                break;
            }
            //đưa dữ liệu loại pizza được chọn thông qua option được chọn từ modal
            if(action.data.pizzaType ==="Seafood"){
                state.newOrder.pizzaType = "Seafood";
                state.pizzaType = "Seafood";
                break;
            }  
            if(action.data.pizzaType ==="Hawaii"){
                state.newOrder.pizzaType = "Hawaii";
                state.pizzaType = "Hawaii";
                break;
            } 
            if(action.data.pizzaType ==="Bacon"){
                state.pizzaType = "Bacon";
                state.newOrder.pizzaType = "Bacon";
                break;
            }
            //đưa dữ liệu nước uống đã chọn vào state.newOrder
            if(action.data.drink !== ""){
                 state.newOrder.drink =  action.data.drink;
                break;
            }    
            break;
        case FETCH_API_VOUCHER:
            if(action.data > -1 && action.data < 100){
              state.newOrder.pizzaSize.thanhTien = state.price - (state.price * action.data /100);
              break;
            }
            break;
        //thu thập thông tin khách hàng trong modal create new order
        case GET_USER_CREATE_NEW_ORDER: 
            state.newOrder.userInfor = action.data;
            break;

        case BUTTON_CREATE_NEW_ORDER:
            state.pendingStatus = false;
            state.statusCreate =false;
            break;

        //truyền thông tin cho toast
        case OPEN_TOAST:
            state.toast =  action.data;
            break;
        default:
            break;
    };
    return {...state}
}