export const FETCH_API_PENDING = "FETCH_API_PENDING";
export const FETCH_API_SUCCESS = "FETCH_API_SUCCESS";
export const FETCH_API_ERROR = "FETCH_API_ERROR";
export const FETCH_API_DRINK = "FETCH_API_DRINK"
export const COUNTING_PAGE = "COUNTING_PAGE";
export const SET_ROW_PAGE ="SET_ROW_PAGE";
export const SET_STATUS_CREATE_MODAL = "SET_STATUS_CREATE_MODAL"
export const SET_STATUS_EDIT_MODAL = "SET_STATUS_EDIT_MODAL"
export const BUTTON_EDIT_HANDLER = "BUTTON_EDIT_HANDLER";
export const CHANGE_STATUS_ORDER_HANDLER ="CHANGE_STATUS_ORDER_HANDLER";
export const CONFIRM_CHANGE_STATUS_ORDER = "CONFIRM_CHANGE_STATUS_ORDER";
export const BUTTON_DELETE_HANDLER ="BUTTON_DELETE_HANDLER";
export const GET_INFO_CREATE_NEW_ORDER ="GET_INFO_CREATE_NEW_ORDER";
export const GET_USER_CREATE_NEW_ORDER = "GET_USER_CREATE_NEW_ORDER";
export const BUTTON_CREATE_NEW_ORDER = "BUTTON_CREATE_NEW_ORDER";
export const BUTTON_CLOSE = "BUTTON_CLOSE";
export const OPEN_TOAST = "OPEN_TOAST";
export const FETCH_API_VOUCHER = "FETCH_API_VOUCHER";
export const CHANGE_STATUS_DELETE = "CHANGE_STATUS_DELETE";
export const CONFIRM_DELETE_ORDER_HANDLER ="CONFIRM_DELETE_ORDER_HANDLER";
