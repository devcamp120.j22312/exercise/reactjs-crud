import { BUTTON_CREATE_NEW_ORDER, BUTTON_DELETE_HANDLER, BUTTON_EDIT_HANDLER, COUNTING_PAGE, FETCH_API_DRINK, FETCH_API_ERROR, 
    FETCH_API_PENDING, FETCH_API_SUCCESS, FETCH_API_VOUCHER, GET_INFO_CREATE_NEW_ORDER, GET_USER_CREATE_NEW_ORDER, OPEN_TOAST, 
    SET_ROW_PAGE, SET_STATUS_CREATE_MODAL,CHANGE_STATUS_ORDER_HANDLER, CONFIRM_CHANGE_STATUS_ORDER, SET_STATUS_EDIT_MODAL, CHANGE_STATUS_DELETE, CONFIRM_DELETE_ORDER_HANDLER } from "../constants/order.constants";

//action toast khi các sự kiện thành công
export const openToast = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: OPEN_TOAST,
            data: data
        })
    }
}
//call api check voucher id 
export const fetchVoucherId = (voucherId) =>{
    return async(dispatch)=>{
        await dispatch({
            type: FETCH_API_PENDING,
        })
        var body = {
            method: 'GET',
            redirect: 'follow'
          };
          try {
            const response =  await fetch("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + voucherId, body);
            const data = await response.json();
            return dispatch({
                type: FETCH_API_VOUCHER,
                data: data.discount
            })
        } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
        }
    }
}
//action khi bấm nút create trong modal create new order
export const createNewOrderHandler = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: FETCH_API_PENDING,
        });
        var infor = data
        const body = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify ({
                kichCo: infor.pizzaSize.kichCo,
                duongKinh: infor.pizzaSize.duongKinh,
                suon: infor.pizzaSize.suon,
                salad: infor.pizzaSize.salad,
                loaiPizza: infor.pizzaType,
                idVourcher: infor.userInfor.idVourcher,
                idLoaiNuocUong: infor.drink,
                soLuongNuoc: infor.pizzaSize.soLuongNuoc,
                hoTen: infor.userInfor.hoTen,
                thanhTien: infor.pizzaSize.thanhTien,
                email: infor.userInfor.email,
                soDienThoai: infor.userInfor.soDienThoai,
                diaChi: infor.userInfor.diaChi,
                loiNhan: infor.userInfor.loiNhan
            }),
            redirect: 'follow'
        };
       try {
            const response =  await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", body);
            const data = await response.json();

            return dispatch({
                type: BUTTON_CREATE_NEW_ORDER,
                data: data
            })
            
          } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
          }
    }
}
//action get information of user on create new order form
export const collectInforUserOnForm = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: GET_USER_CREATE_NEW_ORDER,
            data: data
        });

        //lúc đầu 
        await dispatch(fetchVoucherId(data.userInfor.voucherId))
    }
}
//action get information on create new order form
export const collectInforOnForm = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: GET_INFO_CREATE_NEW_ORDER,
            data: data
        });
    }
}
//actiom khi nút edit được bấm
export const editOrderHandler = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: BUTTON_EDIT_HANDLER,
            data: data,
        })
    }
}
//action khi thay đổi status order
export const changeStatusOrderHandler = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: CHANGE_STATUS_ORDER_HANDLER,
            data: data
        })
    }
}
//action khi bấm nút change status order
export const confirmChangeStatusOrderHandler = (data, orderId) =>{
    return async (dispatch) =>{
        await dispatch({
            type: FETCH_API_PENDING
        });
        const body = {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({trangThai:data}),
            redirect: 'follow'
        };
        try {
            const response =  await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders"+ "/" + orderId, body);
            const data = await response.json();

            return dispatch({
                type: CONFIRM_CHANGE_STATUS_ORDER,
                data: data
            })
            
          } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
          }
    }
}
//actiom khi nút delete được bấm
export const deleteOrderHandler = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: BUTTON_DELETE_HANDLER,
            data: data,
        })
    }
}
//thay đổi trạng thái đóng mở delete modal
export const setStatusDelete = (data)=>{
    return async(dispatch) =>{
        await dispatch({
            type: CHANGE_STATUS_DELETE,
            data: data
        })
    }
}
//xác nhận xóa order
export const confirmDeleteOrderHandler = (data)=>{
    return async (dispatch) =>{
        await dispatch ({
            type: FETCH_API_PENDING,
        });
        var body = {
            method: 'DELETE',
            redirect: 'follow'
          };
        var id = data
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders"+ "/" + id, body);
             dispatch({
                type: CONFIRM_DELETE_ORDER_HANDLER,
                response: response.message
            })
        } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            }) 
        }
    }
}
//thay đổi trạng thái đóng mở create modal
export const setStatusCreate = (data)=>{
    return async(dispatch) =>{
        await dispatch({
            type: SET_STATUS_CREATE_MODAL,
            data: data
        })
    }
}
//thay đổi trạng thái đóng mở edit modal
export const setStatusEdit = (data)=>{
    return async(dispatch) =>{
        await dispatch({
            type: SET_STATUS_EDIT_MODAL,
            data: data
        });
    }
}

//action call api drink 
export const fetchDrinkApi = ()=>{
    return async(dispatch)=>{
        await dispatch({
            type: FETCH_API_PENDING
        });
        var body = {
            method: 'GET',
            redirect: 'follow'
          };
        try {
            const response =  await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", body);
            const data = await response.json();
            //nếu dữ liệu được lấy thành công truyền data drink đã lấy vào data
            return dispatch({
                type: FETCH_API_DRINK,
                data: data
            })
            //trường hợp lỗi
        } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
        }
    }
}
//action xử lý chia trang
export const setRowPage = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: SET_ROW_PAGE,
            data: data
        })
    }
}
//action xử lý tính lượng order load ra theo setting page
export const settingPage = (data)=>{
    return async(dispatch)=>{
        await dispatch({
            type: COUNTING_PAGE,
            data: data,
        })
    }
}
//action xử lý sự kiện call api get order
export const callApiGetAllOrder = () =>{
    return async(dispatch)=>{
        const body = {
            method: "GET",
            redirect: "follow"
        };
        await dispatch({
            type: FETCH_API_PENDING
        });
        try {
            const response =  await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", body);
            const data = await response.json();
            //nếu dữ liệu được lấy thành công truyền data orders đã lấy vào data
             dispatch({
                type: FETCH_API_SUCCESS,
                data: data,
            })
            dispatch({
                type: OPEN_TOAST,
                data: {open: "true",
                        message:"Loading Order Successfully",
                        type:"success"}
            })
            
            //trường hợp lỗi
        } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
        }
    }
}