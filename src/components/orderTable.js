import { CircularProgress, Container, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Button } from "@mui/material"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { callApiGetAllOrder, fetchDrinkApi, setStatusCreate } from "../actions/order.action";
import BodyTable from "./bodyTable";
import CreateNewOrder from "./modal/createNewOrder";
import { DeleteModal } from "./modal/deleteModal";
import { EditStatus } from "./modal/editStatus";
import CustomizedSnackbars from "./modal/toast";
import TablePaginationTab from "./tablePagination";
const OrderTable = ()=>{
    const dispatch = useDispatch();
    const {pendingStatus, statusEdit, toast, statusDelete, statusCreate} = useSelector((reduxData)=>
        reduxData.orderReducer
    );
    //sử dụng use effect để load toàn bộ thông tin order vào table
    useEffect(()=>{
        dispatch(callApiGetAllOrder());
        dispatch(fetchDrinkApi());
    }, []);
    const onBtnCreateHandler =() =>{
        dispatch(setStatusCreate(true))
    };
    return(
        <Container>
            <Grid container mt={5}  mb={5}
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    textAlign="center">
                <Grid item lg={12} md={12} sm={12}  xs={12} >
                    <h1>Order Table Information</h1>
                    <p>Exam Middle</p>
                </Grid>
                
             { pendingStatus ? <CircularProgress/> :  
                <Grid item>
                    <Grid container mb={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    >
                    <Grid item>
                        <Button variant="contained" style={{backgroundColor:"orange"}} onClick={onBtnCreateHandler}>Create Order</Button>
                    </Grid>
                </Grid>
                    <TableContainer>
                        <Table>
                            <TableHead className="header-table">
                                <TableRow>
                                    <TableCell id="header-detail">Order ID</TableCell>
                                    <TableCell id="header-detail">Pizza Size</TableCell>
                                    <TableCell id="header-detail">Pizza Type</TableCell>
                                    <TableCell id="header-detail">Drink</TableCell>
                                    <TableCell id="header-detail">Price</TableCell>
                                    <TableCell id="header-detail">Customer Name</TableCell>
                                    <TableCell id="header-detail">Phone Number</TableCell>
                                    <TableCell id="header-detail">Order Status</TableCell>
                                    <TableCell id="header-detail">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <BodyTable/>
                            </TableBody>
                        </Table>
                        <TablePaginationTab/>
                    </TableContainer>
                </Grid>
                } 
            </Grid>
            {statusCreate ?<CreateNewOrder/>: null}
            {statusEdit?<EditStatus/>:null}
            {toast.open? <CustomizedSnackbars/>: null}
            {statusDelete? <DeleteModal/>:null}
        </Container>
    )
}

export default OrderTable;