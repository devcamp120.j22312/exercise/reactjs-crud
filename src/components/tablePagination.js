import { TablePagination } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { settingPage, setRowPage } from "../actions/order.action";
const TablePaginationTab = () =>{
    const dispatch = useDispatch();
    const { totalOrders, page, rowPerPage} = useSelector((reduxData)=>
        reduxData.orderReducer
    );
    const handleChangePage = (event, newPage) => {
        dispatch(settingPage(newPage));
        };
        
    const handleChangeRowsPerPage = (event) => {
        dispatch(setRowPage(+ event.target.value));
        dispatch(settingPage(0));
        };
    return(
        <>
            <TablePagination
                rowsPerPageOptions={[10, 25, 50, 100]}
                component="div"
                count={totalOrders}
                rowsPerPage={rowPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    )
}
export default TablePaginationTab;