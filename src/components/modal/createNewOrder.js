import { Dialog, Box, Typography, Grid, FormControl, InputLabel, Select, MenuItem, TextField, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openToast, collectInforOnForm, callApiGetAllOrder, collectInforUserOnForm, createNewOrderHandler, fetchVoucherId, setStatusCreate } from "../../actions/order.action";

export default function CreateNewOrder (){
    
    const dispatch = useDispatch();
    const {
        statusCreate,
        newOrder, size , pizzaType, pizzaSize, drink,
        suonNuong, salad, soLuongNuoc, price, newPrice
        } = useSelector((reduxData)=>
        reduxData.orderReducer
    )
    const [newUser, getNewOrder] = useState({
        hoTen: "",
        email: "",
        soDienThoai:"",
        diaChi:"",
        idVourcher: "",
        loiNhan: ""
      })
    const handleChange =(event)=>{
        dispatch(collectInforOnForm({
            [event.target.name] : event.target.value,
        }))
    }
    const handleUserChange = (event) =>{
        getNewOrder({
            ...newUser,
            [event.target.name] : event.target.value
        });
    }
    const onBtnCreateHandler = ()=>{
        
         dispatch(collectInforUserOnForm(newUser)); //1 đẩy thông tin user vào reduce 
         dispatch(fetchVoucherId(newUser.idVourcher));   //2 call api check voucher
         dispatch(createNewOrderHandler(newOrder));  //3 trả về được toàn bộ thông tin cần để POST
         dispatch(openToast({open: "true",
                            message:"Create New Order Successfully",
                            type:"success"}));
         dispatch(callApiGetAllOrder());
    }
    const handleClose = () =>{
        dispatch(setStatusCreate(false));
    }
    return(
        <div>
             <Dialog open= {statusCreate} scroll= "paper" component="form" onSubmit={onBtnCreateHandler}> 
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            Create New Order
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Pizza Size:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel>Select Pizza Size</InputLabel>
                                        <Select value={pizzaSize} name="pizzaSize" label="Select Pizza Size" onChange={handleChange}
                                            required >
                                            <MenuItem value="S">S</MenuItem>
                                            <MenuItem value="M">M</MenuItem>
                                            <MenuItem value="L">L</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Pizza Type:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel>Select Pizza Type</InputLabel>
                                        <Select value={pizzaType} name="pizzaType" label="Select Pizza Type" onChange={handleChange}
                                            required>
                                            <MenuItem value="Seafood">Hải sản</MenuItem>
                                            <MenuItem value="Hawaii">Hawaii</MenuItem>
                                            <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Drink:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel>Select Drink</InputLabel>
                                        <Select value={newOrder.drink} name="drink" label="Select Drink" onChange={handleChange}
                                             required>
                                            {drink.map((value,index)=>{
                                                return (
                                                    <MenuItem value= {value.maNuocUong} key={index}>{value.tenNuocUong}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Đường Kính:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {size}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true, style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Sườn nướng:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {suonNuong}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Salad:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {salad}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Số Lượng Nước:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {soLuongNuoc}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:1}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Thành Tiền:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {newPrice === 0? price : newPrice}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Họ Tên:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    value= {newUser.hoTen}
                                    name= "hoTen"
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{ 
                                        style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    required
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Số Điện Thoại:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    required
                                    type="number"
                                    value= {newUser.soDienThoai}
                                    name ="soDienThoai"
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{ 
                                       style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Email:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    required
                                    type="email"
                                    value= {newUser.email}
                                    name="email"
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{ 
                                        style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Địa Chỉ:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    required
                                    value= {newUser.diaChi}
                                    variant="outlined"
                                    name= "diaChi"
                                    fullWidth
                                    InputProps={{ 
                                        style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Mã voucher:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    value= {newUser.idVourcher}
                                    variant="outlined"
                                    name="idVourcher"
                                    fullWidth
                                    InputProps={{ 
                                        style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Ghi Chú:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    value= {newUser.loiNhan}
                                    name="loiNhan"
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{ 
                                        style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    onChange={handleUserChange}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button type="submit" variant="contained">Create New Order</Button>
                    </DialogActions>
            </Dialog>
        </div>
    )
}