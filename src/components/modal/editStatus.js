import { useDispatch, useSelector } from "react-redux"
import { Dialog, Box, Typography, Grid, FormControl, InputLabel, Select, MenuItem, TextField, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { callApiGetAllOrder, changeStatusOrderHandler, confirmChangeStatusOrderHandler, setStatusEdit } from "../../actions/order.action";

export const EditStatus = () =>{
    const dispatch = useDispatch();
    const {orderInfor, statusEdit, statusOrder} = useSelector((reduxData)=>
        reduxData.orderReducer
    )
    const handleChange =(e) =>{
        dispatch(changeStatusOrderHandler(e.target.value));
    }
    const onBtnEditHandler = ()=>{
        dispatch(confirmChangeStatusOrderHandler(statusOrder, orderInfor.id));
        dispatch(callApiGetAllOrder());
   }
   const handleClose = () =>{
       dispatch(setStatusEdit(false));
   }
    return (
        
        <div>
            <Dialog open= {statusEdit} scroll= "paper" component="form" onSubmit={onBtnEditHandler}> {/* */} 
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            Edit Status
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Order Id:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.orderCode}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true, style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Pizza Size:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.kichCo}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true, style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Sườn nướng:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.suon}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Salad:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.salad}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Số Lượng Nước:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.soLuongNuoc}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:1}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Thành Tiền:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.thanhTien}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:1}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Giảm giá:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.giamGia}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Họ Tên:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.hoTen}
                                    name= "hoTen"
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    required
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Số Điện Thoại:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.soDienThoai}
                                    name ="soDienThoai"
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true , style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Email:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    type="email"
                                    value= {orderInfor.email}
                                    name="email"
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,  style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Địa Chỉ:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.diaChi}
                                    variant="filled"
                                    name= "diaChi"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,  style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Mã voucher:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.idVourcher}
                                    variant="filled"
                                    name="idVourcher"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,   style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} style={{paddingTop: 0}}>
                                <p>Ghi Chú:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.loiNhan}
                                    name="loiNhan"
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,  style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <p>Status:</p>
                            </Grid>
                            <Grid item xs={8}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel>Change Status</InputLabel>
                                        <Select value={statusOrder} name="trangThai" label="Change Status" onChange={handleChange}
                                            >
                                            <MenuItem value="open">Open</MenuItem>
                                            <MenuItem value="confirmed">Confirmed</MenuItem>
                                            <MenuItem value="cancel">Cancel</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                        </Grid>
                    </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button type="submit" variant="contained">Edit Status</Button>
                    </DialogActions>
            </Dialog>
        </div>
    )
}