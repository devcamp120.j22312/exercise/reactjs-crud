import { TableCell, Button, Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { setStatusEdit, setStatusDelete, deleteOrderHandler, editOrderHandler } from "../actions/order.action";
import { StyledTableRow } from "./Style/StyledTableRow";

const BodyTable = () =>{
    const dispatch = useDispatch();
    const {orders, page, rowPerPage} = useSelector((reduxData)=>
        reduxData.orderReducer
    );
    const onBtnEditHandler = (value) =>{
        //truyền giá trị thông tin của order khi bấm nút edit
        dispatch(editOrderHandler(value));
        dispatch(setStatusEdit(true));
    }

    const onBtnDeleteHandler = (value)=>{
        //truyền giá trị 
        dispatch(deleteOrderHandler(value));
        dispatch(setStatusDelete(true));
    }
    return(
        <>
        {orders.slice(page * rowPerPage, page* rowPerPage + rowPerPage)
                .map((value, index)=>{
                    return (
                        <StyledTableRow key={index} lg={12} md={12} sm={12}  xs={12} >
                            <TableCell id="body-table-text">{value.orderCode}</TableCell>
                            <TableCell id="body-table-text">{value.kichCo}</TableCell>
                            <TableCell id="body-table-text">{value.loaiPizza}</TableCell>
                            <TableCell id="body-table-text">{value.idLoaiNuocUong}</TableCell>
                            <TableCell id="body-table-text">{value.thanhTien}</TableCell>
                            <TableCell id="body-table-text">{value.hoTen}</TableCell>
                            <TableCell id="body-table-text">{value.soDienThoai}</TableCell>
                            <TableCell id="body-table-text">{value.trangThai}</TableCell>
                            <TableCell >
                                <Grid container width={170} direction="row" justifyContent="space-evenly" alignItems="center">
                                    <Grid item xs={6}>
                                        <Button id="btn-edit" variant="contained" onClick={()=> onBtnEditHandler(value)}>
                                            Edit
                                        </Button>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Button id="btn-del" variant="contained" onClick={()=> onBtnDeleteHandler(value)}>
                                            Delete
                                        </Button>
                                    </Grid>
                                </Grid>
                            </TableCell>
                        </StyledTableRow>
                    )
                })}
            
        </>
    )
}
export default BodyTable;